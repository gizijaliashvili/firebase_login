package com.example.myapplication.fragment

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment : Fragment (R.layout.fragment_profile) {

    private lateinit var textView: TextView
    private lateinit var textViewEmail: TextView
    private lateinit var textViewUserName : TextView
    private lateinit var textViewUserAge : TextView
    private lateinit var textViewUserNumber : TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init(view)

        registrationListener()

        textView.text = FirebaseAuth.getInstance().currentUser?.uid
        textViewEmail.text = FirebaseAuth.getInstance().currentUser?.email



    }

    private fun init(view: View) {

        textViewUserName = view.findViewById(R.id.textViewUserName)
        textViewUserAge = view.findViewById(R.id.textViewUserAge)
        textViewUserNumber = view.findViewById(R.id.textViewUserNumber)
        textView = view.findViewById(R.id.textViewUid)
        textViewEmail = view.findViewById(R.id.textViewEmail)
    }

    private fun registrationListener(){


    }





}