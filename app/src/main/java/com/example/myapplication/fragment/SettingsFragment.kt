package com.example.myapplication.fragment
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.example.myapplication.LoginActivity
import com.example.myapplication.PasswordChangeActivity
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class SettingsFragment : Fragment(R.layout.fragment_settings) {


    private lateinit var editTextUserName: EditText
    private lateinit var editTextPhone: EditText
    private lateinit var editTextAge: EditText
    private lateinit var buttonSave: Button
    private lateinit var buttonLogout: Button
    private lateinit var buttonPasswordChange: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init(view)

        registrationListener()

    }

    private fun init(view: View) {

        editTextUserName = view.findViewById(R.id.editTextUserName)
        editTextPhone = view.findViewById(R.id.editTextPhone)
        editTextAge = view.findViewById(R.id.editTextAge)
        buttonSave = view.findViewById(R.id.buttonSave)
        buttonLogout = view.findViewById(R.id.buttonLogout)
        buttonPasswordChange = view.findViewById(R.id.buttonPasswordChange)


    }
    private fun registrationListener() {
    buttonLogout.setOnClickListener{
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(activity, LoginActivity::class.java))
        activity?.finish()
    }
    buttonPasswordChange.setOnClickListener{
        startActivity(Intent(activity, PasswordChangeActivity::class.java))
    }

    buttonSave.setOnClickListener{

        }
    }

}