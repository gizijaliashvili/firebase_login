package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.example.myapplication.fragment.SettingsFragment
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {

    private lateinit var buttonBack: Button
    private lateinit var editTextPassword: TextInputEditText
    private lateinit var editTextConfirmPassword: TextInputEditText
    private lateinit var buttonPasswordChange: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)

        init()

        registerListeners()

        goBack()

    }
    private fun init() {

        editTextPassword = findViewById(R.id.editTextPassword)
        editTextConfirmPassword =findViewById(R.id.editTextConfirmPassword)
        buttonPasswordChange = findViewById(R.id.buttonPasswordChange)
        buttonBack = findViewById(R.id.buttonBack)
    }

    private fun registerListeners() {
        buttonPasswordChange.setOnClickListener {

            val newPassword = editTextPassword.text.toString()
            val confirmPassword = editTextConfirmPassword.text.toString()

            if (newPassword.isEmpty() || newPassword.length < 9) {
                Toast.makeText(this, "Password is Empty or less than 9 characters", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            უნდა შეიცავდეს ციფრებს
            if (!newPassword.matches(".*[0-9].*".toRegex())){
                Toast.makeText(this, "Password must contain Numbers", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            უნდა შეიცავდეს დიდ ასოებს
            if (!newPassword.matches(".*[A-Z].*".toRegex())){
                Toast.makeText(this, "Password must contain Uppercase Characters", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            უნდა შეიცავდეს პატარა ასოებს
            if (!newPassword.matches(".*[a-z].*".toRegex())){
                Toast.makeText(this, "Password must contain Lowercase Characters", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            პაროლები უნდა იყოს იგივე
            if  (newPassword != confirmPassword){
                Toast.makeText(this, "Confirm password isn't same", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "Password updated", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }else{
                        Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
    private fun goBack(){
        buttonBack.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

    }

}