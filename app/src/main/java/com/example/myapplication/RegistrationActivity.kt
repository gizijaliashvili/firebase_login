package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {


    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPasswordConfirm: EditText
    private lateinit var buttonRegistration: Button
    private lateinit var buttonBack: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()
        registrationListener()
        goBack()
    }

    private fun init() {

        editTextEmail = findViewById(R.id.editTextEmail)

        editTextPassword = findViewById(R.id.editTextPassword)

        editTextPasswordConfirm = findViewById(R.id.editTextPasswordConfirm)

        buttonRegistration = findViewById(R.id.buttonRegistration)

        buttonBack = findViewById(R.id.buttonBack)

    }

    private fun registrationListener() {
        buttonRegistration.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val confirmPassword = editTextPasswordConfirm.text.toString()

            if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() || password.length < 8) {
                Toast.makeText(this, "Please fill in the blanks", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            უნდა შეიცავდეს ციფრებს
            if (!password.matches(".*[0-9].*".toRegex())){
                Toast.makeText(this, "Password must contain Numbers", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            უნდა შეიცავდეს დიდ ასოებს
            if (!password.matches(".*[A-Z].*".toRegex())){
                Toast.makeText(this, "Password must contain Uppercase Characters", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            უნდა შეიცავდეს პატარა ასოებს
            if (!password.matches(".*[a-z].*".toRegex())){
                Toast.makeText(this, "Password must contain Lowercase Characters", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
//            პაროლენო უნდა იყოს იგივე
            if  (password != confirmPassword){
                Toast.makeText(this, "Confirm password isn't same", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this, LoginActivity::class.java))
                    } else {
                        Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
    private fun goBack(){
        buttonBack.setOnClickListener{
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

    }

}